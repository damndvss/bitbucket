$(document).ready(function () {
    $("[data-role=header]").toolbar();
    $("[data-role=header]").navbar();    
     
   
});
$( document ).on( "pagecreate", function() {
    $( "#dark-theme input" ).on( "change", function( event ) {
         var darkTheme = $( "#dark-theme input:checked" ).attr( "id" );
        $( "#page").removeClass( "ui-page-theme-b" ).addClass( "ui-page-theme-" + darkTheme );
    });
});

